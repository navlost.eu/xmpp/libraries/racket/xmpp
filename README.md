# XMPP library for Racket

An [XMPP](https://xmpp.org/) library for use with [Racket](https://racket-lang.org/),
a Scheme implementation.

Its low-level interface was inspired by [xmppjs](https://github.com/xmppjs/xmpp.js/).

The starting point for the code was an [older, partial implementation](https://github.com/poi519/nan-xmpp).

## Basic example

```scheme
(require xmpp)
(define jid (string->jid "user@example.net"))
(define pass "s3cret!")
(define conn (xmpp-connect-client jid pass))
(xmpp-send conn '(presence ()))

(xmpp-receive conn)

; Result:
;'(presence
;  ((from "user@example.net/racket-4035") (to "user@example.net/racket-4035") (xml:lang "en"))
;  (x ((xmlns "vcard-temp:x:update")) (photo ())))
```
