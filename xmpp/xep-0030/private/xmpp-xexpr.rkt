#lang racket/base

(require racket/string
         xmpp/core/jid
         xmpp/xep-0030/datatypes)

(provide
 xmpp->xexpr)

(define (xmpp->xexpr v)

  (define (non-empty attrs)
    (filter (λ (v)
              (non-empty-string? (cadr v)))
            attrs))

  (cond
    ((xmpp-disco-info-identity? v)
     `(identity
       ((name ,(xmpp-disco-info-identity-name v))
        (category ,(xmpp-disco-info-identity-category v))
        (type ,(xmpp-disco-info-identity-type v)))))

    ((xmpp-disco-info-feature? v)
     `(feature
       ((var ,(xmpp-disco-info-feature-var v))
        (node ,(xmpp-disco-info-feature-node v)))))

    ((xmpp-disco-info? v)
     ; NOTE: We ignore FORM data for now
     (let* ((node (xmpp-disco-info-node v))
            (attrs (if (non-empty-string? node)
                       `((xmlns "http://jabber.org/protocol/disco#info") (node ,node))
                       `((xmlns "http://jabber.org/protocol/disco#info")))))
       `(query
         ,attrs
         ,@(map xmpp->xexpr (xmpp-disco-info-identities v))
         ,@(map xmpp->xexpr (xmpp-disco-info-features v)))))

    ((xmpp-disco-item? v)
     `(item
       ,(non-empty `((jid ,(jid->string (xmpp-disco-item-jid v)))
                     (node ,(xmpp-disco-item-node v))
                     (name ,(xmpp-disco-item-name v))))))

    ((xmpp-disco-items? v)
     (let ((node (xmpp-disco-items-node v)))
       (if (non-empty-string? node)
           `(query
             ((xmlns "http://jabber.org/protocol/disco#items") (node ,node))
             ,@(map xmpp->xexpr (xmpp-disco-items-items v)))
           `(query
             ((xmlns "http://jabber.org/protocol/disco#items"))
             ,@(map xmpp->xexpr (xmpp-disco-items-items v))))))

    (else v)))
