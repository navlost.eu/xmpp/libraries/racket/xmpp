#lang scribble/manual

@require[@for-label[xmpp/core
                    xml
                    racket/base]
         scribble/core
         scribble/eval]

@title{XMPP Error Functions}
@author[@author+email["Navlost" "racket@navlost.eu" #:obfuscate? #t]]

@(define helper-eval (make-base-eval))
@interaction-eval[#:eval helper-eval
                  (require xml "main.rkt")]


@defmodule[xmpp/errors]

This package implements functions for constructing XMPP error stanzas.

@defproc[(service-unavailable ...)
         (xexpr/c)]{
 Produce a @hyperlink["https://datatracker.ietf.org/doc/html/rfc6120#section-8.3.3.19"]{@tt{<service-unavailable/>}}
           stanza.

 @examples[
 #:eval helper-eval
 (service-unavailable '(text
                        ((xmln "urn:ietf:params:xml:ns:xmpp-stanzas")
                         (xml:lang "en"))
                        "I did not understand your request"))
 ]
}
