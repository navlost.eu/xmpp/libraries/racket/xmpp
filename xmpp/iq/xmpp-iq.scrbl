#lang scribble/manual

@require[@for-label[xmpp/core
                    xml
                    racket/base]
         scribble/core
         scribble/eval]

@title{XMPP IQ (Info/Query) Functions}
@author+email["Navlost" "racket@navlost.eu" #:obfuscate? #t]

@(define helper-eval (make-base-eval))
@interaction-eval[#:eval helper-eval
                  (require xml "main.rkt")]


@defmodule[xmpp/iq]

This package implements functions for dealing with
@hyperlink["https://xmpp.org/rfcs/rfc6120.html#stanzas-semantics-iq"]{IQ} (Info/Query)
stanzas.

@defproc[(make-iq-reply (#:type type string? "result") (iq xexpr/c) (body xexpr/c) ...)
         (xexpr/c)]{
 Generates an @racket[xexpr] which is a result stanza corresponding to @racket[iq]
 with any extra arguments in the body of the stanza.

 This is done by copying the @tt{id} attribute, switching the @tt{to} and @tt{from}
 attributes and setting @tt{type=@racket[type]} (defaults to @tt{"result"}).

 @#reader scribble/comment-reader
 [examples
 #:eval helper-eval
 ; An example incoming IQ
 (define iq (string->xexpr
             (string-append
              "<iq type='get' id='abcde' "
              "to='user@example.net' from='user@example.com'>"
              "<query xmlns='http://jabber.org/protocol/disco#info'/>"
              "</iq>")))

 ; A result IQ based off the previous query.
 (make-iq-reply iq
                 '(query
                   ((xmlns "http://jabber.org/protocol/disco#info"))
                   (identity ((type "registered") (category "account")))))
 ]
}
