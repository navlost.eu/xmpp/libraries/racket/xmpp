#lang info
(define collection "xmpp")
(define deps '("base"))

(define build-deps '("scribble-lib" "racket-doc" "rackunit-lib"))
(define scribblings '(("xmpp-iq.scrbl" ())))

(define pkg-desc "XMPP IQ functions")
(define version "0.1")
(define pkg-authors '(navlost))
