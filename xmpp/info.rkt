#lang info

(define deps '("base"))
(define scribblings '(("xmpp.scrbl" ())))
(define pkg-desc "XMPP modules")
(define version "0.1")
(define pkg-authors '(navlost))
(define license 'GPL-3.0-only)
