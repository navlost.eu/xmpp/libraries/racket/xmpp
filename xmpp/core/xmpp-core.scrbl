#lang scribble/manual

@require[@for-label[xmpp/core
                    xml
                    racket/tcp
                    racket/base]
         scribble/core
         scribble/eval]

@title{XMPP Core Functions}
@author{@author+email["Navlost" "racket@navlost.eu" #:obfuscate? #t]}

@(define href-xmpp.org "https://xmpp.org/")
@(define href-rfc-jid "https://datatracker.ietf.org/doc/html/rfc6122")

@(define helper-eval (make-base-eval))
@interaction-eval[#:eval helper-eval
                  (require "main.rkt")]


@defmodule[xmpp/core]

This package implements core elements required by other higher-level
@hyperlink[href-xmpp.org]{XMPP} packages.

@;defmodule[xmpp/core/id]

@defproc[(xmpp-id)
         (string?)]{
 Generates a string suitable for use as a stanza ID.
}

@;defmodule[xmpp/core/jid]

@defstruct*[jid
            ((localpart string?)
             (domainpart string?)
             (resourcepart string?))]{
 Representation of a @hyperlink[href-rfc-jid]{JID}, an XMPP address.

 A JID consists of a @racket[localpart], a @racket[domainpart]
 and a @racket[resourcepart], in that order. The @racket[localpart]
 is optional and if present, it must precede the @racket[domainpart]
 and is separated from it by an @tt{@"@"} sign. Likewise, the
 @racket[resourcepart] is also optional, follows the @racket[domainpart]
 and is separated from it by a @tt{/} (slash) character. A valid JID always
 has at least a @racket[domainpart].
}

@defproc[(invalid-jid)
         (jid?)]{
 Creates an instance of @racket[jid] representing an invalid JID.

 This is mainly useful when populating other data structures that
 require @racket[jid] values, before we are able to obtain a valid
 JID from the user.
}

@defproc[(valid-jid? (jid jid?))
         (boolean?)]{
 Predicate returning @racket[#t] if @racket[jid] is a valid JID,
 @racket[#f] otherwise.

 @examples[
 #:eval helper-eval
 (valid-jid? (jid "user" "example.net" "laptop"))

 (valid-jid? (invalid-jid))
 ]
}

@defproc[(bare-jid (jid jid?))
         (jid?)]{
 Given a JID, return the
 @hyperlink["https://datatracker.ietf.org/doc/html/rfc6120#section-1.4"]{bare JID}.

 @examples[
 #:eval helper-eval
 (bare-jid (jid "user" "example.net" "laptop"))
 ]
}

@defproc[(bare-jid-equal? (jid0 jid?) (jid1 jid?))
         (boolean?)]{
 Compare two JIDs for equality, disregarding their resource part.

 @margin-note{To compare two full JIDs for equality, use @racket[equal?].}

 @examples[
 #:eval helper-eval
 (equal? (jid "user" "example.net" "laptop")
         (jid "user" "example.net" "workstation"))

 (bare-jid-equal? (jid "user" "example.net" "laptop")
                  (jid "user" "example.net" "workstation"))

 (bare-jid-equal? (jid "user" "example.net" "laptop")
                  (jid "user" "example.com" "laptop"))
 ]
}

@defproc[(jid/string? (v any/c))
         (boolean?)]{
 Predicate returning @racket[#t] if @racket[v] is either a
 @racket[jid] or a @racket[string], whether or not it can be parsed into
 a valid JID.

 @examples[
 #:eval helper-eval
 (jid/string? (jid "user" "example.net" "laptop"))

 (jid/string? (invalid-jid))

 (jid/string? "user@example.net/laptop")

 (jid/string? "")

 (jid/string? 33)
 ]
}

@defproc[(jid->string (jid jid?))
         (string?)]{
 Return a string representation of a @racket[jid].

 @examples[
 #:eval helper-eval
 (jid->string (jid "user" "example.net" "resource"))
 ]
}

@defproc[(jid→string (jid jid?))
         (string?)]{
 An alias for @racket[jid->string].
}

@defproc[(jid/string->string (v (or/c jid? string?)))
         (string?)]{
 Return a string representation of a @racket[jid], or
 @racket[v] if it is already a string.

 This can be used to provide some flexibility when
 accepting inputs into procedures requiring JIDs
 as strings.
}

@defproc[(jid/string→string (v (or/c jid? string?)))
         (string?)]{
 An alias for @racket[jid/string->string].
}

@defproc[(string->jid (str (and/c string? non-empty-string?)))
         (jid?)]{
 Try to parse a string into a @racket[jid].

 @examples[
 #:eval helper-eval
 (string->jid "user@example.net/resource")
 ]
}

@defproc[(string→jid (str (and/c string? non-empty-string?)))
         (jid?)]{
 An alias for @racket[string->jid].
}

@defstruct*[xmpp-connection
            ([host string?]
             [port port-number?]
             [jid jid?]
             [passwd string?]
             [i-port input-port?]
             [o-port output-port?]
             [custodian custodian?]
             [buffer (listof xexpr/c)]
             [encryption symbol?])]{
 Mutable structure that holds the details of an XMPP session.
}
