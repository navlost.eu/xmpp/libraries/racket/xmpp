#lang scribble/manual

@require[@for-label[xmpp
                    racket/base]
         scribble/core]

@title{XMPP Library}
@author[@author+email["Navlost" "racket@navlost.eu" #:obfuscate? #t]]

This package implements
@hyperlink["https://xmpp.org/extensions/xep-0030.html"]{XMPP Service Discovery (XMPP-0030)},
an @hyperlink["https://xmpp.org/"]{XMPP} protocol extension for discovering information
about other XMPP entities.

The package consists of a number of subpackages, each encapsulating an aspect of the protocol. These are:

@itemlist[
@item{@other-doc['(lib "xmpp/core/xmpp-core.scrbl")]}
@item{@other-doc['(lib "xmpp/transport/xmpp-transport.scrbl")]}
@item{@other-doc['(lib "xmpp/errors/xmpp-errors.scrbl")]}
@item{@other-doc['(lib "xmpp/iq/xmpp-iq.scrbl")]}
]
