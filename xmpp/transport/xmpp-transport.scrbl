#lang scribble/manual

@require[@for-label[xmpp/transport
                    xmpp/core
                    xml
                    racket/tcp
                    racket/contract
                    racket/base]
         scribble/core
         scribble/eval]

@title{XMPP Connection}

@author[@author+email["Navlost" "racket@navlost.eu" #:obfuscate? #t]]

@(define href-xmpp.org "https://xmpp.org/")
@(define href-rfc-xmpp-tls "https://www.rfc-editor.org/rfc/rfc7590.html")

@(define helper-eval (make-base-eval))
@interaction-eval[#:eval helper-eval
                  (require xmpp/core "main.rkt")]


@margin-note{
 @smaller{Find the source at @url{https://gitlab.com/navlost.eu/xmpp/libraries/racket/xmpp}.}
}

@defmodule[xmpp/transport]

This package implements functions to connect and authenticate with an
@hyperlink[href-xmpp.org]{XMPP} server.

Example:

@racketblock[
 (require xmpp/transport)
 (define jid (string->jid "user@example.net"))
 (define pass "s3cret!")
 (define conn (xmpp-connect-client jid pass))
 (xmpp-send conn '(presence ()))
 ]

After connecting, the library assigns a
unique JID resource automatically:

@racketinput[
 (jid->string (xmpp-connection-jid conn))
 ]
@(racketresultblock
  "user@example.net/racket-1547"
  )


@section{Creating and managing connections}


@defproc[(xmpp-connect-client (jid jid?)
                       (passwd string?)
                       (host string? (jid-domainpart jid))
                       (port port-number? XMPP-C2S-PORT))
         (xmpp-connection?)]{
 This method:
 @itemlist[
 @item{Creates a @racket[xmpp-connection] object;}
 @item{connects to the server, either @racket[host] if provided
   or else @racket[(jid-domainpart jid)], either on the default
   XMPP port (5222) or on @racket[port];}
 @item{upgrades the connection to @hyperlink[href-rfc-xmpp-tls]{TLS};}
 @item{authenticates the user with the provided @racket[jid] and
   @racket[passwd];}
 @item{randomises the @racket[jid-resourcepart] and binds it; and}
 @item{returns the @racket[xmpp-connection] object, which is used by
   every other function needing to interact with the server.}
 ]
}

@defproc[(xmpp-connect-component (host string?)
                       (port port-number?)
                       (domain string?)
                       (secret string?))
         (xmpp-connection?)]{
 This method creates an
 @hyperlink["https://xmpp.org/extensions/xep-0114.html"]{XEP-0114}
 component connection.

 @itemlist[
 @item{@racket[host] is the hostname of the XMPP server to connect to}
 @item{@racket[port] is the port number to connect to, according to the
  server configuration (there is no standard port number or range for
  components).}
 @item{@racket[domain] is the domain name for the component itself.
  Technically, this is a JID without either a local part or a resource part.}
 @item{@racket[secret] is the shared secret between the XMPP server and the
  component, used during the handshake.}
 ]
}

@defproc[(xmpp-stop! (conn xmpp-connection?))
         (any)]{
 Closes the connection to the server.
}



@section{Sending data}

@defproc[(xmpp-send (conn xmpp-connection?) (stanza xexpr/c))
         (any)]{
 Sends a stanza.

 Example:

 @racketinput[
 ; Send an initial <presence/> stanza.
 (xmpp-send conn '(presence))
 ]
}

@section{Receiving data}

There are a number of approaches to receiving data. Incoming stanzas
may be processed synchronously (either blocking the execution thread
or not) or via callbacks. Callbacks may be one-off or repeating.

@defproc[(xmpp-receive (conn xmpp-connection?))
         (or/c xexpr/c #f)]{
 Gets the next available stanza from the connection buffer.

 Returns @racket[#f] if no stanzas are available.

 Example:

 @racketinput[
 (xmpp-receive conn)
 ]
 @(racketresult #f)
}

@defproc[(xmpp-receive-blocking (conn xmpp-connection?))
         (xexpr/c)]{
 Gets the next available stanza from the connection buffer.
 If the buffer is empty, block until a new stanza is received
 or the connection is lost.

 Example:

 @racketinput[
 (xmpp-receive-blocking conn)
 ]
 @;(racketresult #f)
}

@section{Asynchronous operation}

A common way to interact with XMPP is by registering callbacks.
These must conform to the contract @racket[xmpp-handler/c] and
get called whenever a stanza arrives.

@defproc[(xmpp-set-handler (conn xmpp-connection?) (handler xmpp-handler/c default-handler))
         (any)]{
 Sets a handler that gets called every time a new stanza arrives.

 If called with no arguments, the default handler is installed, which is
 @emph{required} when using the functions in the @secref["callbacks"] section.

 Otherwise, the argument must be a function taking a stanza (@racket[xexpr/c])
 and a @racket[(struct connnection)] as arguments. The return value is ignored.

 @margin-note{Only one handler may be set per connection.
  However, it is possible to have arbitrarily many callbacks.}

 Example:

 @racketinput[
 (xmpp-set-handler conn (λ (stanza conn)
                          (displayln
                           (~a "Received a new stanza of type "
                               (car stanza)))))
 ]
}

@defproc[(xmpp-stop-handler (conn xmpp-connection?))
         (any)]{
 Remove any previously installed handler for @racket[conn].
}

@defproc[(xmpp-handler (conn xmpp-connection?))
         (or/c xmpp-handler/c #f)]{
 Return the installed handler for @racket[conn], if any.
 Otherwise, return @racket[#f].
}


@section[#:tag "callbacks"]{Callbacks}

For the procedures in this section to operate as intended, the default
handler must be installed on the target connection, via
@racket[(xmpp-set-handler conn)].

@defproc[(xmpp-set-callback (conn xmpp-connection?) (callback xmpp-callback/c))
         (any)]{
 Install a callback on @racket[conn].
}

@defproc[(xmpp-set-callback/once (conn xmpp-connection?)
                                 (callback xmpp-callback/once/c))
         (any)]{
 Install a callback that uninstalls itself when it signals to the
 handler that it's done by returning @racket[#t]. These callbacks
 are the basis for the interactions described in @secref["interact"].

 Example: @margin-note*{Remember to install the default handler.}

 @#reader scribble/comment-reader
 (racketinput
 (xmpp-set-handler conn)

 ; Callback that matches any IQ stanzas of
 ; type "result", prints a message and returns #t.
 (define (iq-result-handler-once stanza conn)
   (match stanza
     ( (list 'iq
             (list-no-order '(type "result") _ ...) _ ...)
       (displayln "An IQ of type result has been seen.")
       #t)
     (_ (void))))

 (xmpp-set-callback conn iq-result-handler-once)

 ; Two pings are sent, but only the first response to arrive
 ; will cause the callback to run.

 (xmpp-send conn
            `(iq ((from ,(jid->string (xmpp-connection-jid conn)))
                  (to ,(jid-domainpart (xmpp-connection-jid conn)))
                  (id ,(xmpp-id)))
                 (ping ((xmlns "urn:xmpp:ping")))))

 (xmpp-send conn
            `(iq ((from ,(jid->string (xmpp-connection-jid conn)))
                  (to ,(jid-domainpart (xmpp-connection-jid conn)))
                  (id ,(xmpp-id)))
                 (ping ((xmlns "urn:xmpp:ping")))))
 )
 @racketresult[
 "An IQ of type result has been seen."
 ]
}


@defproc[(xmpp-unset-callback (conn xmpp-connection?)
                                 (callback xmpp-callback/c))
         (any)]{
 If installed, @racket[callback] will be removed from this
 connection. It doesn't matter if the callback was installed
 by @racket[xmpp-set-callback] or @racket[xmpp-set-callback/once].
}

@defproc[(xmpp-callbacks (conn xmpp-connection?))
         (listof xmpp-callback/c)]{
 Return the list of callbacks currently installed in this
 connection.
}


@section[#:tag "interact"]{Responding to remote interactions}

@defproc[(xmpp-send/callback (conn xmpp-connection?)
                             (stanza xexpr/c)
                             (callback xmpp-callback/c))
         (any)]{
 Send a stanza and call @racket[callback] when a response
 is received.

 A response is defined as an incoming stanza of the same
 type and with the same @racketlink[xmpp-id]{id}.

 This is implemented by wrapping @racket[callback] with
 the right logic to detect responses and registering the
 new function via @racket[xmpp-callback/once].

 Example:

 @racketinput[
 (define ping
   `(iq ((from ,(jid->string (xmpp-connection-jid conn)))
                   (to ,(jid-domainpart (xmpp-connection-jid conn)))
                   (id ,(xmpp-id)))
                  (ping ((xmlns "urn:xmpp:ping")))))

 (xmpp-send/callback conn ping (λ (response conn)
                                 (displayln "PONG!")))
 ]
 @racketresult[
 "PONG!"
 ]
}

@defproc[(xmpp-send/await (conn xmpp-connection?)
                             (stanza xexpr/c)
                             (callback xmpp-callback/c))
         (any)]{
 Send a stanza and wait for the result of @racket[callback].

 This is similar to @racket[xmpp-send/callback] except that
 the call will block until @racket[callback] returns.
}

@section[#:tag "contracts"]{Contracts}

@margin-note{
 @larger{👉🏽}
 @secref[#:doc '(lib "scribblings/guide/guide.scrbl") "contracts"]
  in @other-manual['(lib "scribblings/guide/guide.scrbl")]
  introduces contracts.
}

@defthing[#:kind "contract" stanza/c
  contract?]{
 Defined as:

 @racketblock[(or/c xexpr/c #f)]

 The return value of @racket[xmpp-receive] (but not
 @racket[xmpp-receive-blocking] which always returns
 an @racket[xexpr/c]).
}

@defthing[#:kind "contract" xmpp-handler/c
  contract?]{
 Defined as:

 @racketblock[(-> stanza/c any)]

 A procedure suitable for @racket[xmpp-set-handler].
}

@defthing[#:kind "contract" xmpp-callback/c
  contract?]{
 Defined as:

 @racketblock[(-> stanza/c xmpp-connection? any)]

 A procedure suitable for @racket[xmpp-set-callback].
}

@defthing[#:kind "contract" xmpp-callback/once/c
  contract?]{
 Defined as:

 @racketblock[(-> stanza/c xmpp-connection? boolean?)]

 A procedure suitable for @racket[xmpp-set-callback/once].
}
